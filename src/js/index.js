const button = document.querySelector(".menu__button");
const menu = document.querySelector(".menu-button-list");
const line1 = document.querySelector(".line1");
const line2 = document.querySelector(".line2");
const line3 = document.querySelector(".line3");
button.addEventListener("click", () => {
  menu.classList.toggle("button-list-active");
  line1.classList.toggle("line1__active");
  line2.classList.toggle("line2__active");
  line3.classList.toggle("line3__active");
});

// --------------------------------------------------------
// --------------------------------------------------------

var splide = new Splide(".splide", {
  type: "loop",
  padding: "5rem",
});

splide.mount();
